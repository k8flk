#  FLK Makefile
#
#  Copyright (C) 1998 Lars Krueger
#
#  This file is part of FLK.
#
#  FLK is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# Change to match your system.
INSTALL_LIBDIR=/usr/local/lib/flk
INSTALL_BINDIR=/usr/local/bin

# If DEBUGGING is not true, loader messages are suppressed and the loader is
# optimized.
DEBUGGING?=false

# If DELAY_FLUSH is true, the experimental terminal flushing code is compiled.
DELAY_FLUSH?=true

# Don't modify anything below this line. ---------------------------------------
ifeq (${DEBUGGING},true)
DELAY_FLUSH=false
endif

CFLAGS=-std=gnu99 -Wall -pipe -D__UNIX__ -D__LINUX__ \
       -DINSTALL_DIR=\"${INSTALL_LIBDIR}/\" \
       -DINSTALL_BIN_DIR=\"${INSTALL_BINDIR}/\"

ifeq (${DEBUGGING},true)
CFLAGS+= -g
else
CFLAGS+= -DNDEBUG -O2
endif

ifeq (${DELAY_FLUSH},true)
CFLAGS+=-DDELAY_FLUSH
endif

INCLUDE=-I.
LDFLAGS=-L/usr/lib -lncurses -rdynamic -ldl

ifneq (${DEBUGGING},true)
LDFLAGS+=-s
endif

# Version setting
VERSION=1.3
SRCS_C=kernel/flk.c

#SRCDIRCMD=': SRCDIR S"' `pwd`'/" ; '
SRCDIRCMD=': SRCDIR S" ./" ;'

OBJECTS=${SRCS_C:%.c=%.o}

SRCS=fth/flkasm.fs fth/flkdict.fs fth/flkenv.fs fth/flkexcep.fs fth/flkhcomp.fs fth/flkhigh.fs \
     fth/flkinput.fs fth/flkio.fs fth/flkkern.fs fth/flkkey_unix.fs fth/flkmeta.fs \
     fth/flkprim.fs fth/flksys.fs fth/flktcomp.fs fth/flkstring.fs fth/extend.fs fth/flktools.fs \
     fth/flkfloat.fs fth/array.fs fth/defer.fs fth/flklib.fs fth/flklevel2.fs fth/flkopt.fs \
     fth/flktopt.fs fth/flkhiex.fs fth/flkcfstack.fs

all: flk

flk default.flk: flkkern flk.flk ${SRCS}
	cp flkkern flk
	./flkkern fth/extend.fs

flkkern: ${OBJECTS}
	${LINK.o} $^ -o $@

flk.flk: ${SRCS} fth/linuxsc.fs
	./tools/f

clean:
	rm .depends flk.flk default.flk flk flkkern ${OBJECTS}

depend:
	rm .depends
	make .depends

.depends:
	${CC} -M ${CFLAGS} ${INCLUDE} ${SRCS_C} > .depends

install: default.flk flk
	install -d ${INSTALL_LIBDIR}
	cp default.flk ${INSTALL_LIBDIR}
	install -d ${INSTALL_BINDIR}
	cp flk ${INSTALL_BINDIR}


include .depends
