/* FLK loader program

  Copyright (C) 1998 Lars Krueger

  This file is part of FLK.

  FLK is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
 * $Id: flk.c,v 1.25 1998/08/30 10:50:59 root Exp $
 *****************************************************************************
 * $Log: flk.c,v $
 * Revision 1.25  1998/08/30 10:50:59  root
 * added diagnostics
 *
 * Revision 1.24  1998/07/15 16:38:23  root
 * TERM=dumb bugss removed
 *
 * Revision 1.23  1998/07/09 19:16:41  root
 * fixed relocation to enable code size changes
 *
 * Revision 1.22  1998/06/24 05:30:46  root
 * corrected terminal setup and cleanup
 *
 * Revision 1.21  1998/06/03 07:55:16  root
 * corrected flush on exit
 *
 * Revision 1.20  1998/06/01 18:05:39  root
 * changed image search order (argv[0] doesn't contain a path)
 *
 * Revision 1.19  1998/06/01 17:51:42  root
 * GET-CWD added
 *
 * Revision 1.18  1998/05/24 18:43:16  root
 * delayed flush corrected
 *
 * Revision 1.17  1998/05/24 15:41:26  root
 * Delayed flush can be turned off
 *
 * Revision 1.16  1998/05/23 19:23:54  root
 * delayed flushing of stdout
 *
 * Revision 1.15  1998/05/21 19:24:49  root
 * XForms support
 *
 * Revision 1.14  1998/05/17 08:27:09  root
 * script mode, ODOES>
 *
 * Revision 1.13  1998/05/16 16:19:24  root
 * direct terminfo access
 *
 * Revision 1.12  1998/05/09 21:47:05  root
 * S, renamed to ,C
 * ,S included
 *
 * Revision 1.11  1998/05/02 15:05:55  root
 * CREATE-FILE corrected
 *
 * Revision 1.10  1998/05/01 18:13:33  root
 * Image search order changed to:
 * executable, ./flk.flk, default.flk
 *
 * Revision 1.9  1998/05/01 18:11:25  root
 * GNU license text added
 * comments checked
 *
 * Revision 1.8  1998/04/30 09:42:25  root
 * other image search order
 *
 * Revision 1.7  1998/04/29 18:26:32  root
 * SYSTEM, BELL, ALERT
 * TYPE fixed (was printf like, now puts like)
 *
 * Revision 1.6  1998/04/27 18:41:42  root
 * directory primitives
 *
 * Revision 1.5  1998/04/25 11:03:28  root
 * added flk.c to check in list
 *
 * Revision 1.4  1998/04/11 11:58:14  root
 * REPOSITION-FILE checked
 *
 * Revision 1.3  1998/04/10 14:42:50  root
 * new header format to ease SAVE-SAYSTEM
 *
 * Revision 1.2  1998/04/07 20:10:33  root
 * final .flk format, OS calls, image search
 *
 */
#include <assert.h>
#include <dirent.h>
#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#define NCURSES_INTERNALS
#include <ncurses.h>
#include <term.h>


static int goobers = 0;


static DIR *glob_dir = NULL;
static struct dirent *glob_dirent = NULL;
static struct termios shell_ios;
static struct termios prog_ios;

#ifdef DELAY_FLUSH
# define FLUSHBUFFERLEN  (5000)
static char flushBuffer[FLUSHBUFFERLEN];
static unsigned flushBufferInd;


static void myFlush (void) {
  write(1, flushBuffer, flushBufferInd);
  flushBufferInd = 0;
}


static void myputp (const char *s) {
  unsigned int l;
  //
  assert(s);
  l = strlen(s);
  if (l+flushBufferInd >= FLUSHBUFFERLEN) {
    signal(SIGALRM, SIG_IGN);
    myFlush();
  }
  memcpy(flushBuffer+flushBufferInd, s, l);
  flushBufferInd += l;
}


static void flushHandler (int sig) {
  myFlush();
}


static void startFlusher (void) {
  if (flushBufferInd) {
    struct itimerval to;
    //
    to.it_value.tv_sec = 0;
    to.it_value.tv_usec = 40000;
    to.it_interval.tv_sec = 0;
    to.it_interval.tv_usec = 0;
    setitimer(ITIMER_REAL, &to, NULL);
    signal(SIGALRM, flushHandler);
  }
}

#else
# define myputp  putp
# define startFlusher(...)  ((void)0)
#endif


/*==============================global constants==============================*/
static const unsigned char headerID[] = "FLK Image 1.1";
#define HEADER_ID_LEN  (sizeof(headerID)-1)

#define FTRUE  ((unsigned int)(~0))

#define STACK  unsigned int _stptr_ = 0

#define POP(var,erg)    do { erg=stack[_stptr_].var; _stptr_++; } while (0)
#define PUSH(var, val)  do {_stptr_--; stack[_stptr_].var=val; } while (0)
#define POPSTRING(erg)  do { unsigned int len; char *s; POP(uval, len); POP(caddr, s); erg=makeCString(s, len); } while (0)
#define DPUSH(x)        do { PUSH(nval, x); PUSH(nval, (x<0) ? -1 : 0); } while (0)
#define UDPUSH(x)       do { PUSH(uval, x); PUSH(uval, 0); } while (0)


/*==============================global variables==============================*/
static unsigned int *ReloTable;
static unsigned int *ForthDataStack, *ForthCallStack;
static unsigned int *Image;
static unsigned int MemorySize = 0;


/*==============================static functions==============================*/
/* This could be done more eloquently, right? */
static void printErrorBanner (void) {
  printf("FLK Error: ");
}


static char *makeCString (const char *fstr, unsigned int len) {
  char *res = (char *)malloc(len+1);
  //
  assert(res);
  memcpy(res, fstr, len);
  res[len] = 0;
  return res;
}


static void setupTerm (void) {
  setupterm(NULL, 1, NULL);
  def_shell_mode();
  tcgetattr(1, &shell_ios);
  prog_ios = shell_ios;
  prog_ios.c_lflag &= ~(ICANON | ECHO);
  prog_ios.c_iflag &= ~(ICRNL | INLCR);
  tcsetattr(1, TCSANOW, &prog_ios);
  tcflow(1, TCOON);
  def_prog_mode();
#ifdef DELAY_FLUSH
  flushBufferInd = 0;
#else
  setbuf(stdout, 0);
#endif
}


static void cleanupTerm (void) {
  if (glob_dir) closedir(glob_dir);
  reset_shell_mode();
  tcsetattr(1, TCSADRAIN, &shell_ios);
#ifdef DELAY_FLUSH
  signal(SIGALRM, SIG_IGN);
  myFlush();
#endif
}


/*==============================system primitives=============================*/
typedef union {
  unsigned int uval;
  int nval;
  char *caddr;
  void *vaddr;
} Cell;

typedef void (*SystemPrimitive) (Cell *stack);


/* x -- */
static void EMIT (Cell *stack) {
  STACK;
  int x;
  char xx[2];
  //
  POP(nval, x);
  switch (x) {
    case -1: /* backspace */
      if (!cursor_left) myputp("\010"); else myputp(cursor_left);
      break;
    default:
      xx[0] = x;
      xx[1] = 0;
      myputp(xx);
      break;
  }
  startFlusher();
}


/* -- char */
static void KEY (Cell *stack) {
  STACK;
  int key;
  //
  do {
    key = getchar();
  } while (!(32 <= key && key < 127));
  PUSH(nval, key);
}


/* c-addr n  --  */
static void TYPE (Cell *stack) {
  STACK;
  char *str;
  //
  POPSTRING(str);
  myputp(str);
  startFlusher();
  free(str);
}


/*  --  */
static void CR (Cell *stack) {
  if (newline) {
    myputp(newline);
  } else {
    myputp(cursor_down);
    myputp("\r");
  }
  startFlusher();
}


/* x y --  */
static void AT_XY (Cell *stack) {
  STACK;
  int y, x;
  //
  POP(uval, y);
  POP(uval, x);
  if (cursor_address) {
    myputp(tgoto(cursor_address, x, y));
    startFlusher();
  }
}


/*  -- flag  */
static void KEY_IF (Cell *stack) {
  STACK;
  fd_set set;
  struct timeval tv;
  //
  FD_ZERO(&set);
  FD_SET(1, &set);
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  PUSH(nval, select(2, &set, NULL, NULL, &tv) ? FTRUE : 0);
}


/*  --  */
static void PAGE (Cell *stack) {
  if (clear_screen) {
    myputp(clear_screen);
    startFlusher();
  }
}


/*  -- u  */
static void EKEY (Cell *stack) {
  STACK;
  int key = getchar();
  PUSH(nval, key);
}


/* u  -- u false | char true */
static void EKEY_2_CHAR (Cell *stack) {
  STACK;
  int key;
  //
  POP(nval, key);
  PUSH(nval, key);
  PUSH(uval, (32 <= key && key < 127) ? FTRUE : 0);
}


/* -- flag */
static void EKEY_IF (Cell *stack) {
  KEY_IF(stack);
}


/* -- flag */
static void EMIT_IF (Cell *stack) {
  STACK;
  PUSH(uval, FTRUE);
}


/* u -- */
static void MS (Cell *stack) {
  STACK;
  unsigned int ms;
  struct timeval tv;
  //
  POP(uval, ms);
#ifdef DELAY_FLUSH
  signal(SIGALRM, SIG_IGN);
  myFlush();
#endif
  tv.tv_sec = ms/1000;
  tv.tv_usec = (ms%1000)*1000;
  select(0, 0, 0, 0, &tv);
}


/* -- sec min hr day mn yr */
static void TIME_DATE (Cell *stack) {
  STACK;
  time_t t = time(NULL);
  struct tm *tms = localtime(&t);
  //
  assert(tms);
  PUSH(uval, tms->tm_sec);
  PUSH(uval, tms->tm_min);
  PUSH(uval, tms->tm_hour);
  PUSH(uval, tms->tm_mday);
  PUSH(uval, tms->tm_mon+1);
  PUSH(uval, tms->tm_year+1900);
}


/* u -- a-addr ior */
static void ALLOCATE (Cell *stack) {
  STACK;
  unsigned cnt;
  void *p;
  //
  POP(uval, cnt);
  p = malloc(cnt);
  PUSH(vaddr, p);
  PUSH(uval, p ? 0 : -9);
}


/* u -- ior  */
static void _FREE (Cell *stack) {
  STACK;
  void *buf;
  //
  POP(vaddr, buf);
  free(buf);
  PUSH(uval, 0);
}


/* addr u  -- addr2 ior */
static void RESIZE (Cell *stack) {
  STACK;
  unsigned int u;
  void *p, *p2;
  //
  POP(uval, u);
  POP(vaddr, p);
  p2 = realloc(p, u);
  PUSH(vaddr, p2);
  PUSH(nval, p2 ? 0 : -9);
}


/* code -- */
static void BYE (Cell *stack) {
  STACK;
  int ec;
  //
  POP(nval, ec);
  cleanupTerm();
  exit(ec);
}


/* -- rows */
static void SCR_ROWS (Cell *stack) {
  STACK;
  PUSH(nval, LINES);
}


/* -- cols */
static void SCR_COLS (Cell *stack) {
  STACK;
  PUSH(nval, COLS);
}


/* fid -- ior  */
static void CLOSE_FILE (Cell *stack) {
  STACK;
  int fid;
  //
  POP(nval, fid);
  if (close(fid)) PUSH(nval, -37); else PUSH(nval, 0);
}


#define FM_RO   (1)
#define FM_RW   (2)
#define FM_BIN  (4)
#define FM_WO   (8)

static int c_mode (int forth_mode) {
  int erg = 0;
  //
  if (forth_mode&FM_RO) erg = O_RDONLY;
  else if (forth_mode&FM_RW) erg = O_RDWR;
  else if (forth_mode&FM_WO) erg = O_WRONLY;
  return erg;
}


/* addr u mode -- fid ior */
static void CREATE_FILE (Cell *stack) {
  STACK;
  int mode, fid, nmode;
  char *name;
  //
  POP(nval, mode);
  POPSTRING(name);
  nmode = c_mode(mode);
  fid = open(name, nmode|O_CREAT, 0666);
  PUSH(nval, fid);
  if (fid == -1) PUSH(nval, -37); else PUSH(nval, 0);
  free(name);
}


/* addr n -- ior  */
static void DELETE_FILE (Cell *stack) {
  STACK;
  char *name;
  POPSTRING(name);
  //
  if (unlink(name) < 0) PUSH(nval, -37); else PUSH(nval, 0);
  free(name);
}


/* fid  -- ud ior */
static void FILE_POSITION (Cell *stack) {
  STACK;
  int fid, where;
  //
  POP(nval, fid);
  where = lseek(fid, 0, SEEK_CUR);
  PUSH(uval, where);
  PUSH(nval, 0);
  if (where == -1) PUSH(nval, -37); else PUSH(nval, 0);
}


/* fid -- ud ior */
static void FILE_SIZE (Cell *stack) {
  STACK;
  int fid;
  off_t where;
  //
  POP(nval, fid);
  where = lseek(fid, 0, SEEK_CUR);
  if (where == -1) {
    PUSH(nval, 0);
    PUSH(nval, 0);
    PUSH(nval, -37);
  } else {
    off_t end = lseek(fid, 0, SEEK_END);
    PUSH(uval, end);
    PUSH(nval, 0);
    if (-1 == lseek(fid, where, SEEK_SET)) PUSH(nval, -37); else PUSH(nval, 0);
  }
}


/* addr u mode -- fid ior */
static void OPEN_FILE (Cell *stack) {
  STACK;
  int mode, fid, nmode;
  char *name;
  //
  POP(nval, mode);
  POPSTRING(name);
  nmode = c_mode(mode);
  fid = open(name, nmode);
  PUSH(nval, fid);
  if (fid == -1) PUSH(nval, -38); else PUSH(nval, 0);
  free(name);
}


/* addr u fid -- u2 ior */
static void READ_FILE (Cell *stack) {
  STACK;
  int fid;
  unsigned int len, lread;
  char *buf;
  //
  POP(nval, fid);
  POP(uval, len);
  POP(caddr, buf);
  lread = read(fid, buf, len);
  PUSH(uval, lread);
  if (lread == -1) PUSH(nval, -37); else PUSH(nval, 0);
}


/* addr u fid -- u2 flag ior */
static void READ_LINE (Cell *stack) {
  STACK;
  int fid, seek_res, len, lread, i;
  char *buf;
  //
  POP(nval, fid);
  POP(uval, len);
  POP(caddr, buf);
  lread = read(fid, buf, len);
  for(i = 0; i < lread && buf[i] != '\n'; i++);
  seek_res = lseek(fid, i-lread+1, SEEK_CUR);
  PUSH(nval, i);
  PUSH(nval, lread ? FTRUE : 0);
  PUSH(nval, (lread == -1 || seek_res == -1) ? -37 : 0);
}


/* ud fid -- ior  */
static void REPOSITION_FILE (Cell *stack) {
  STACK;
  int fid, res = -1;
  unsigned int udh, udl;
  //
  POP(nval, fid);
  POP(uval, udh);
  POP(uval, udl);
  if (!udh) res = lseek(fid, udl, SEEK_SET);
  if (res < 0) PUSH(nval, -37); else PUSH(nval, 0);
}


/* ud fid -- ior  */
static void RESIZE_FILE (Cell *stack) {
  STACK;
  int fid, res = -1;
  unsigned int udh, udl, flen;
  //
  POP(nval, fid);
  POP(uval, udh);
  POP(uval, udl);
  if (!udh) {
    flen = lseek(fid, 0, SEEK_END);
    if ((int)flen != -1) {
      if (udl > flen) {
        /* write some */
        char *buf = malloc(udl-flen);
        if (buf) {
          res = write(fid, buf, udl-flen);
          free(buf);
        }
      } else res = ftruncate(fid, udl);
    }
  }
  if (res < 0) PUSH(nval, -37); else PUSH(nval, 0);
}


/* a1 u1 a2 u2 -- ior  */
static void RENAME_FILE (Cell *stack) {
  STACK;
  char *from, *to;
  //
  POPSTRING(from);
  POPSTRING(to);
  if (rename(from, to) < 0) PUSH(nval, -37); else PUSH(nval, 0);
  free(to);
  free(from);
}


/* addr u fid -- ior  */
static void WRITE_FILE (Cell *stack) {
  STACK;
  int fid;
  unsigned int len;
  char *buf;
  //
  POP(nval, fid);
  POP(uval, len);
  POP(caddr, buf);
  if (write(fid, buf, len) != len) PUSH(nval, -37); else PUSH(nval, 0);
}


/* addr u fid -- ior  */
static void WRITE_LINE (Cell *stack) {
  STACK;
  int fid;
  unsigned int len, lwr;
  char *buf, cr;
  //
  POP(nval, fid);
  POP(uval, len);
  POP(caddr, buf);
  lwr = write(fid, buf, len);
  if (lwr == len) {
    cr = '\n';
    if (1 != write(fid, &cr, 1)) lwr = -1;
  }
  if (lwr != len) PUSH(nval, -37); else PUSH(nval, 0);
}


/* addr u -- x ior */
static void FILE_STATUS (Cell *stack) {
  STACK;
  char *name;
  static struct stat buf;
  int erg;
  //
  POPSTRING(name);
  erg = stat(name, &buf);
  PUSH(vaddr, &buf);
  if (erg == 0) PUSH(nval, 0); else PUSH(nval, -38);
  free(name);
}


/* fid -- ior  */
static void FLUSH_FILE (Cell *stack) {
  STACK;
  int fid;
  //
  POP(nval, fid);
  if (-1 == fsync(fid)) PUSH(nval, -37); else PUSH(nval, 0);
  PUSH(nval, 0);
}


/* fstat -- flag */
static void IS_DIRECTORY (Cell *stack) {
  STACK;
  struct stat *buf;
  //
  POP(vaddr, buf);
  if (S_ISDIR(buf->st_mode)) PUSH(uval, FTRUE); else PUSH(uval, 0);
}


/* -- */
static void SCR_BELL (Cell *stack) {
  if (bell) {
    myputp(bell);
    startFlusher();
  }
}


/* -- */
static void SCR_FLASH (Cell *stack) {
  if (flash_screen) {
    myputp(flash_screen);
    startFlusher();
  }
}


/* addr len -- retval */
static void SYSTEM (Cell *stack) {
  STACK;
  char *cmd;
  //
  POPSTRING(cmd);
  reset_shell_mode();
  tcsetattr(1, TCSADRAIN, &shell_ios);
#ifdef DELAY_FLUSH
  signal(SIGALRM, SIG_IGN);
  myFlush();
#endif
  PUSH(nval, system(cmd));
  reset_prog_mode();
  tcsetattr(1, TCSADRAIN, &prog_ios);
  free(cmd);
}


#define READDIR  do { \
  if (!glob_dir) { \
    PUSH(uval, 0); \
    PUSH(uval, 0); \
    PUSH(uval, 0); \
  } else { \
    glob_dirent = readdir(glob_dir); \
    if (!glob_dirent) { \
      PUSH(uval, 0); \
      PUSH(uval, 0); \
      PUSH(uval, 0); \
      PUSH(uval, 0); \
      closedir(glob_dir); \
      glob_dir=0; \
    } else { \
      PUSH(caddr, glob_dirent->d_name); \
      PUSH(uval, strlen(glob_dirent->d_name)); \
      PUSH(uval, FTRUE); \
    } \
  } \
} while (0)


/* addr1 u1 -- addr2 u2 flag1 */
static void FIND_FIRST (Cell *stack) {
  STACK;
  char *dir;
  //
  POPSTRING(dir);
  if (glob_dir) closedir(glob_dir);
  glob_dir = opendir(dir);
  READDIR;
  free(dir);
}


/* -- addr u flag1 */
static void FIND_NEXT (Cell *stack) {
  STACK;
  READDIR;
}


/* -- secs usecs */
static void TIME_OF_DAY (Cell *stack) {
  STACK;
  struct timeval tv;
  //
  gettimeofday(&tv, NULL);
  PUSH(uval, tv.tv_sec);
  PUSH(uval, tv.tv_usec);
}


/* addr len flag -- lib */
static void OPENLIB (Cell *stack) {
  STACK;
  char *name;
  int flags;
  void *lib;
  //
  POP(nval, flags);
  POPSTRING(name);
  lib = dlopen(name, flags);
  PUSH(vaddr, lib);
  free(name);
}


/* -- cstring */
static void LIBERROR (Cell *stack) {
  STACK;
  PUSH(caddr, (char *)dlerror());
}


/* lib addr len -- fct */
static void LIBSYMBOL (Cell *stack) {
  STACK;
  void *lib, *sym;
  char *name;
  //
  POPSTRING(name);
  POP(vaddr, lib);
  sym = dlsym(lib, name);
  PUSH(vaddr, sym);
  free(name);
}


/* lib -- */
static void CLOSELIB (Cell *stack) {
  STACK;
  void *lib;
  //
  POP(vaddr, lib);
  dlclose(lib);
}


/* -- bool-array-addr */
static void TERM_BOOL (Cell *stack) {
  STACK;
  PUSH(vaddr, cur_term->type.Booleans);
}


/* -- short-array-addr */
static void TERM_NUMBER (Cell *stack) {
  STACK;
  PUSH(vaddr, cur_term->type.Numbers);
}


/* -- char*-array-addr */
static void TERM_STRING (Cell *stack) {
  STACK;
  PUSH(vaddr, cur_term->type.Strings);
}


/* -- cwd */
static void GET_CWD (Cell *stack) {
  STACK;
  static char buf[PATH_MAX];
  //
  getcwd(buf, sizeof(buf));
  strcat(buf, "/");
  PUSH(vaddr, buf);
}


/* -- flag */
static void IS_GOOBERSP (Cell *stack) {
  STACK;
  PUSH(nval, goobers);
}


/* -- bindir */
static void GET_BIN_PATH (Cell *stack) {
  STACK;
  static char buf[PATH_MAX];
  //
  memset(buf, 0, sizeof(buf));
  if (readlink("/proc/self/exe", buf, sizeof(buf)-1) < 0) {
    strcpy(buf, "./");
  } else {
    char *p = strrchr(buf, '/');
    //
    if (p != NULL) p[1] = 0; else strcpy(buf, "./");
  }
  PUSH(vaddr, buf);
}


static const SystemPrimitive primitives[] = {
  (SystemPrimitive)(51), // # of primitives
  EMIT, KEY, TYPE, CR, AT_XY, KEY_IF, PAGE, EKEY, EKEY_2_CHAR, EKEY_IF,
  EMIT_IF, MS, TIME_DATE, ALLOCATE, _FREE, RESIZE, BYE, CLOSE_FILE,
  CREATE_FILE, DELETE_FILE, FILE_POSITION, FILE_SIZE, OPEN_FILE, READ_FILE,
  READ_LINE, REPOSITION_FILE, RESIZE_FILE, RENAME_FILE, WRITE_FILE,
  WRITE_LINE, FILE_STATUS, FLUSH_FILE, SCR_ROWS, SCR_COLS, FIND_FIRST,
  FIND_NEXT, IS_DIRECTORY, SCR_BELL, SCR_FLASH, SYSTEM, TIME_OF_DAY, OPENLIB,
  LIBERROR, LIBSYMBOL, CLOSELIB, TERM_BOOL, TERM_NUMBER, TERM_STRING, GET_CWD,
  IS_GOOBERSP, GET_BIN_PATH
};


/* Fill the global variables with values. */
enum {
  HA_INIT_DATASTACK = 0, /* initial value of EBP */
  HA_INIT_CALLSTACK, /* initial value of ESP */
  HA_RELTABLE, /* address of relocation table */
  HA_LOADER, /* loader table address */
  HA_HERE_LIMIT, /* first byte after data area */
  HA_CHERE_LIMIT, /* first byte after code area */
  HA_IMAGE_BASE, /* base address of image */
  HA_INIT_HERE, /* initial value of HERE */
  HA_INIT_CHERE, /* initial value of CHERE */
  HA_BOOT_CODE, /* address of boot code */
  HA_ENTRY, /* address of entry_point */
  HA_CODESIZE, /* number of cells in code area */
  HA_DATASIZE, /* number of cells in data area */
  HA_DATA_CELLS, /* initial size of data stack in cells */
  HA_CALL_CELLS /* initial size of call stack in cells */
}; /*14*/


#define READCELL(x)  read(inFile, &x, sizeof(unsigned int))

static int readImage (int inFile, unsigned int offs) {
  unsigned int imglen, caLen, daLen, reloAlloc, reloCnt, caUse, daUse, imgBase, oldCaLen/*, oldDaLen*/;
  /* inFile is correctly positioned */
#define FP  printf("%d\n", lseek(inFile, 0, SEEK_CUR));
  READCELL(reloCnt);
  reloAlloc = (3*reloCnt)/2;
  ReloTable = (unsigned int *)calloc(reloAlloc+2, sizeof(unsigned int));
  if (!ReloTable) return 0;
  ReloTable[0] = reloAlloc;
  ReloTable[1] = reloCnt;
  read(inFile, &ReloTable[2], reloCnt*sizeof(unsigned int));
  READCELL(caLen);
  READCELL(daLen);
  READCELL(caUse);
  READCELL(daUse);
  READCELL(imgBase);
  if (goobers) {
    printf("OK.\n");
  }
  if (MemorySize > caLen) {
    caLen = MemorySize;
    if (goobers) {
      printf("Memory size override for code area used.\n");
    }
  }
  if (MemorySize > daLen) {
    daLen = MemorySize;
    if (goobers) {
      printf("Memory size override for data area used.\n");
    }
  }
  imglen = caLen+daLen;
  //Image = (unsigned int *)calloc(imglen, sizeof(char));
  Image = (unsigned int *)mmap(NULL, imglen, PROT_EXEC|PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
  if (Image == MAP_FAILED) {
    if (goobers) {
      printf("Could not allocate memory for image.\n");
    }
    return 0;
  }
  memset(Image, 0, imglen);
  if (goobers) {
    printf("code area length: %d bytes; data area length: %d bytes; relocation records: %u\n", caLen, daLen, reloCnt);
  }
  read(inFile, Image, caUse);
  read(inFile, Image+caLen/sizeof(unsigned int), daUse);
  ForthDataStack = (unsigned int *)calloc(Image[HA_DATA_CELLS]+1, sizeof(unsigned int));
  if (ForthDataStack == NULL) return 0;
  ForthCallStack = (unsigned int *)calloc(Image[HA_CALL_CELLS], sizeof(unsigned int));
  if (ForthCallStack == NULL) return 0;
  Image[HA_INIT_DATASTACK] = (unsigned int) ForthDataStack+Image[HA_DATA_CELLS]*sizeof(unsigned int);
  Image[HA_INIT_CALLSTACK] = (unsigned int) ForthCallStack+Image[HA_CALL_CELLS]*sizeof(unsigned int);
  Image[HA_RELTABLE] = (unsigned int)ReloTable;
  Image[HA_LOADER] = (unsigned int)primitives;
  Image[HA_HERE_LIMIT] = (unsigned int)Image+imglen;
  Image[HA_CHERE_LIMIT] = (unsigned int)Image+caLen;
  Image[HA_IMAGE_BASE] = (unsigned int)Image;
  Image[HA_INIT_CHERE] = (unsigned int)Image+caUse;
  Image[HA_INIT_HERE] = (unsigned int)Image+daUse+caLen;
  oldCaLen = Image[HA_CODESIZE];
  //oldDaLen = Image[HA_DATASIZE];
  Image[HA_CODESIZE] = caLen;
  Image[HA_DATASIZE] = daLen;
  for (unsigned int i = 0; i < ReloTable[1]; ++i) {
    unsigned int *relAddr, origVal;
    char *cImage = (char *)Image;
    unsigned int relItem = ReloTable[i+2];
    //
    if (relItem >= oldCaLen) {
      relItem = relItem-oldCaLen+caLen;
      ReloTable[i+2] = relItem;
    }
    relAddr = (unsigned int *)(&cImage[relItem]);
    origVal = *relAddr-imgBase;
    if (origVal >= oldCaLen) {
      *relAddr = (unsigned int)Image+origVal-oldCaLen+caLen;
    } else {
      *relAddr = (unsigned int)Image+origVal;
    }
  }
  return 1;
}


/* Jump to the start of the image. */
typedef void (*GoForth) (int argc, char **argv);


static void startImage (int argc, char **argv) {
  GoForth goForth = (GoForth)Image[HA_BOOT_CODE];
  //
  goForth(argc, argv);
}


/* Free all allocated space. */
#define DELETE(x)  if (x) { free(x); x=NULL; }

static void freeImage (void) {
  DELETE(ReloTable);
  DELETE(Image);
  DELETE(ForthCallStack);
  DELETE(ForthDataStack);
}


static int idFound (int fi, unsigned int offs) {
  unsigned char buf[HEADER_ID_LEN];
  //
  lseek(fi, offs, SEEK_SET);
  read(fi, buf, HEADER_ID_LEN);
  for (unsigned int i = 0; i < HEADER_ID_LEN; ++i) if (buf[i] != (headerID[i]|0x80)) return 0;
  return 1;
}


static int findImageHeader (int fi, unsigned int *offs) {
  unsigned int len = lseek(fi, 0, SEEK_END);
  //
  assert(offs);
  for (unsigned int i = 0; i < len; ++i) {
    if (idFound(fi, i)) {
      *offs = i;
      return 1;
    }
  }
  return 0;
}


/* The function needs no return value, because it does not return, if it is
   successful. Otherwise you may try to run a different image. */
static void runImage (const char *fileName, int argc, char **argv) {
  /* we want to open a binary file, therefore we use handles, not FILEs,
     because some systems (e.g. NeXTStep 3.1 for Intel 386) can't open binary
     FILES, only binary handles. */
  int inFile;
  unsigned imageOk, offs;
  //
  assert(fileName);
  if (goobers) {
    printf("Trying image file '%s': ", fileName);
    fflush(stdout);
  }
  inFile = open(fileName, O_RDONLY);
  if (inFile == -1) {
    /* some error occured; tell what happened and return */
    if (goobers) {
      printf("%s\n", strerror(errno));
    }
    return;
  }
  imageOk = 0;
  if (findImageHeader(inFile, &offs)) {
    if (readImage(inFile, offs)) imageOk = 1;
    else if (goobers) printf("Error loading image.\n");
  } else if (goobers) printf("Contains no image.\n");
  close(inFile);
  if (imageOk) {
    setupTerm();
    if (goobers) {
      printf("Image started.\n");
    }
    /* this function never returns */
    startImage(argc, argv);
  }
  /* clean up */
  freeImage();
}


static void removeOptions (int *argc, char **argv, int pos, int cnt) {
  if (pos+cnt >= *argc) {
    *argc = 0;
  } else {
    for (int f = pos+cnt; f < *argc; ++f) argv[f-cnt] = argv[f];
    (*argc) -= cnt;
  }
}


static void processOptions (int *argc, char **argv) {
  for (int i = 1; i < *argc; ++i) {
    if (strcmp(argv[i], "-m") == 0) {
      if (i+1 < *argc) {
        MemorySize = (atoi(argv[i+1])+3)&~3;
        removeOptions(argc, argv, i, 2);
        --i;
        if (goobers) {
          printf("Memory size overrride: %d bytes\n", MemorySize);
        }
      }
    } else if (strcmp(argv[i], "-goobers") == 0) {
      goobers = 1;
      removeOptions(argc, argv, i, 1);
      --i;
    } else {
      return;
    }
  }
}


/*================================main program================================*/
int main (int argc, char **argv) {
  char buf[4096]; // this should be enough for everyone
  //
  processOptions(&argc, argv);
  /* Try to find an image in this program. */
  memset(buf, 0, sizeof(buf));
  if (readlink("/proc/self/exe", buf, sizeof(buf)-1) < 0) {
    runImage(argv[0], argc, argv);
  } else {
    runImage(buf, argc, argv);
  }
  strcpy(buf, INSTALL_BIN_DIR);
  strcat(buf, argv[0]);
  runImage(buf, argc, argv);
  runImage("flk.flk", argc, argv);
  runImage(INSTALL_DIR "default.flk", argc, argv);
  /* No image could be found. This must be a User-too-stupid error. */
  printErrorBanner();
  printf("I could not load any image. This is all your fault.\n");
  return 1;
}
